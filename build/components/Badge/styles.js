'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactNative = require('react-native');

var _constants = require('../../constants');

exports.default = styles = _reactNative.StyleSheet.create({
  text: {
    color: 'white',
    fontWeight: _constants.theme.font.weight.bold,
    fontSize: _constants.theme.font.size.small
  },
  container: {
    width: 40,
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 12.5
  }
});