'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Chip = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _constants = require('../../constants');

var _Typography = require('../Typography');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Chip = function Chip(props) {
  var backgroundColor = props.backgroundColor,
      text = props.text,
      textColor = props.textColor,
      onDelete = props.onDelete,
      opacity = props.opacity;

  var renderDelete = onDelete();
  return _react2.default.createElement(
    _reactNative.View,
    { style: [_styles2.default.container, { backgroundColor: backgroundColor, opacity: opacity }] },
    _react2.default.createElement(
      _Typography.Typography,
      { style: [_styles2.default.text, { color: textColor }] },
      text
    ),
    renderDelete !== null && _react2.default.createElement(
      _reactNative.TouchableOpacity,
      { style: _styles2.default.button, onPress: onDelete },
      _react2.default.createElement(_reactNative.View, { style: { width: 15, height: 15, backgroundColor: 'red' } }),
      ' //need to change to icon'
    )
  );
};

Chip.propTypes = {
  backgroundColor: _propTypes2.default.string,
  text: _propTypes2.default.string,
  textColor: _propTypes2.default.string,
  onDelete: _propTypes2.default.func,
  opacity: _propTypes2.default.number
};

Chip.defaultProps = {
  backgroundColor: _constants.theme.color.blue.medium,
  text: 'Bachelors',
  textColor: 'white',
  onDelete: function onDelete() {
    return null;
  },
  opacity: 1
};

exports.Chip = Chip;