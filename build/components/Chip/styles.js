'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactNative = require('react-native');

var _constants = require('../../constants');

exports.default = styles = _reactNative.StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderRadius: 4,
    height: 24,
    padding: _constants.theme.spacing.unit / 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: _constants.theme.font.size.small,
    fontWeight: _constants.theme.font.weight.medium,
    alignSelf: 'center'
  },
  button: {
    marginLeft: _constants.theme.spacing.unit,
    alignItems: 'center',
    justifyContent: 'center'
  }
});