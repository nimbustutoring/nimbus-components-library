'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NotificationDot = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NotificationDot = function NotificationDot(props) {
  var color = props.color;


  return _react2.default.createElement(_reactNative.View, { style: [_styles2.default.dot, { backgroundColor: color }] });
};

NotificationDot.propTypes = {
  color: _propTypes2.default.string
};

NotificationDot.defaultProps = {
  color: _constants.theme.color.red.light
};

exports.NotificationDot = NotificationDot;