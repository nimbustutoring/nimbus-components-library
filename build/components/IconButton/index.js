'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconButton = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var IconButton = function IconButton(props) {
  var backgroundColor = props.backgroundColor,
      children = props.children,
      disabled = props.disabled,
      onPress = props.onPress;


  return _react2.default.createElement(
    _reactNative.TouchableOpacity,
    {
      disabled: disabled,
      onPress: onPress,
      style: [_styles2.default.container, { backgroundColor: backgroundColor }],
      hitSlop: { top: 5, left: 5, bottom: 5, right: 5 }
    },
    children
  );
};

IconButton.propTypes = {
  children: _propTypes2.default.any.isRequired,
  backgroundColor: _propTypes2.default.string,
  disabled: _propTypes2.default.bool,
  onPress: _propTypes2.default.func
};

IconButton.defaultProps = {
  backgroundColor: 'transparent',
  disabled: false,
  onPress: function onPress() {}
};

exports.IconButton = IconButton;