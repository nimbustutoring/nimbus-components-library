'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactNative = require('react-native');

var _constants = require('../../constants');

var SIZE = _constants.theme.spacing.unit * 4;

exports.default = styles = _reactNative.StyleSheet.create({
  container: {
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    width: SIZE,
    height: SIZE,
    padding: _constants.theme.spacing.unit / 2
  }
});