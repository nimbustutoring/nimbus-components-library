'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Card = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Card = function Card(props) {
  var children = props.children,
      style = props.style,
      showShadow = props.showShadow;

  var shadow = showShadow ? { shadowOffset: { width: 5, height: 5 },
    shadowColor: _constants.theme.color.default.dark,
    shadowOpacity: 0.2
  } : {};

  return _react2.default.createElement(
    _reactNative.View,
    { style: [style, _styles2.default.container, shadow] },
    children
  );
};

Card.propTypes = {
  children: _propTypes2.default.node,
  style: _propTypes2.default.any
};

Card.defaultProps = {
  children: '',
  style: {}
};

exports.Card = Card;