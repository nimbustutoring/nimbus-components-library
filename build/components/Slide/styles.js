'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactNative = require('react-native');

var _constants = require('../../constants');

exports.default = styles = _reactNative.StyleSheet.create({
  container: {
    backgroundColor: _constants.theme.color.default.light,
    width: '100%',
    height: _constants.theme.spacing.unit / 1.6,
    borderRadius: 10,
    justifyContent: 'center'
  },
  buttonOutter: {
    backgroundColor: _constants.theme.color.blue.medium,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonInner: {
    backgroundColor: 'white'
  },
  animatedContainer: {
    flex: 1,
    width: 0,
    backgroundColor: _constants.theme.color.blue.medium,
    justifyContent: 'center'
  }
});