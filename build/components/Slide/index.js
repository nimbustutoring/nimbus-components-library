'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Slide = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _constants = require('../../constants');

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Slide = function (_React$Component) {
  _inherits(Slide, _React$Component);

  function Slide(props) {
    _classCallCheck(this, Slide);

    var _this = _possibleConstructorReturn(this, (Slide.__proto__ || Object.getPrototypeOf(Slide)).call(this, props));

    _this.state = {
      size: 20,
      pan: new _reactNative.Animated.ValueXY(),
      scale: new _reactNative.Animated.Value(1)
    };
    _this._onPressIn = _this._onPressIn.bind(_this);
    _this._onPressOut = _this._onPressOut.bind(_this);
    return _this;
  }

  _createClass(Slide, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var _this2 = this;

      var pan = this.state.pan;


      this._val = { x: 0, y: 0 };
      pan.addListener(function (value) {
        return _this2._val = value;
      });

      this.panResponder = _reactNative.PanResponder.create({
        onMoveShouldSetResponderCapture: function onMoveShouldSetResponderCapture() {
          return true;
        },
        onMoveShouldSetPanResponderCapture: function onMoveShouldSetPanResponderCapture() {
          return true;
        },

        //initially set x and y valuej
        onPanResponderGrant: function onPanResponderGrant(e, gestureState) {
          pan.setOffset({ x: pan.x._value, y: 0 });
          pan.setValue({ x: 0, y: 0 });
          _reactNative.Animated.spring(_this2.state.scale, { toValue: 1.2, friction: 3 }).start();
        },

        onPanResponderMove: function onPanResponderMove(e, gestureState) {
          console.log(pan.x);
          pan.x._value === undefined ? null : _reactNative.Animated.event([null, { dx: pan.x, dy: 0 }])(e, gestureState);
        },

        onPanResponderRelease: function onPanResponderRelease(e, _ref) {
          var vx = _ref.vx,
              vy = _ref.vy;

          // Flatten the offset to avoid erratic behavior
          pan.flattenOffset();
          _reactNative.Animated.spring(_this2.state.scale, { toValue: 1, friction: 3 }).start();
        }
      });
    }
  }, {
    key: '_onPressIn',
    value: function _onPressIn() {
      this.setState({ size: 22 });
    }
  }, {
    key: '_onPressOut',
    value: function _onPressOut() {
      this.setState({ size: 20 });
    }
  }, {
    key: '_renderButtonThumb',
    value: function _renderButtonThumb() {
      var size = this.state.size;

      return _react2.default.createElement(
        _reactNative.View,
        {
          style: [_styles2.default.buttonOutter, { width: size, height: size, borderRadius: size / 2 }]
        },
        _react2.default.createElement(_reactNative.View, {
          style: [_styles2.default.buttonInner, { width: size / 2, height: size / 2, borderRadius: size / 4 }] })
      );
    }
  }, {
    key: '_renderButton',
    value: function _renderButton() {
      var _state = this.state,
          pan = _state.pan,
          scale = _state.scale;
      var dark = _constants.theme.color.blue.dark;
      var _ref2 = [pan.x],
          translateX = _ref2[0];

      var animatedStyle = { transform: [{ translateX: translateX }, { scale: scale }] };

      return _react2.default.createElement(
        _reactNative.Animated.View,
        _extends({}, this.panResponder.panHandlers, {
          style: [animatedStyle, _styles2.default.animatedContainer]
        }),
        this._renderButtonThumb()
      );
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        _reactNative.View,
        { style: _styles2.default.container },
        this._renderButton()
      );
    }
  }]);

  return Slide;
}(_react2.default.Component);

;

Slide.propTypes = {};

Slide.defaultProps = {};

exports.Slide = Slide;