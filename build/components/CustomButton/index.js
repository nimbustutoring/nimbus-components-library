'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CustomButton = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _GradientBackground = require('../GradientBackground');

var _Typography = require('../Typography');

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _constants = require('../../constants');

var _lodash = require('lodash');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CustomButton = function CustomButton(props) {
  var title = props.title,
      onPress = props.onPress,
      width = props.width,
      height = props.height,
      disabled = props.disabled,
      secondary = props.secondary,
      secondaryTextColor = props.secondaryTextColor,
      isStudent = props.isStudent;


  var renderPrimaryButton = function renderPrimaryButton() {
    var _theme$color = _constants.theme.color,
        blue = _theme$color.blue,
        orange = _theme$color.orange;

    var start = isStudent ? orange.gradientStart : blue.gradientStart;
    var end = isStudent ? orange.gradientEnd : blue.gradientEnd;
    return _react2.default.createElement(_GradientBackground.GradientBackground, {
      color1: start,
      color2: end
    });
  };

  return _react2.default.createElement(
    _reactNative.View,
    null,
    !secondary && _react2.default.createElement(
      _reactNative.View,
      { style: [_styles2.default.container, { width: width, height: height }] },
      _react2.default.createElement(
        _reactNative.TouchableOpacity,
        {
          disabled: disabled,
          onPress: onPress,
          style: [_styles2.default.primaryButton, { width: width, height: height }]
        },
        _react2.default.createElement(
          _Typography.Typography,
          { style: _styles2.default.primaryText },
          (0, _lodash.toUpper)(title)
        )
      ),
      renderPrimaryButton()
    ),
    secondary && _react2.default.createElement(
      _reactNative.TouchableOpacity,
      {
        style: _styles2.default.secondaryButton,
        onPress: onPress
      },
      _react2.default.createElement(
        _Typography.Typography,
        { style: [_styles2.default.text, { color: secondaryTextColor }] },
        title
      )
    )
  );
};

CustomButton.propTypes = {
  title: _propTypes2.default.string,
  onPress: _propTypes2.default.func,
  width: _propTypes2.default.any,
  height: _propTypes2.default.any,
  disabled: _propTypes2.default.bool,
  secondary: _propTypes2.default.bool,
  secondaryTextColor: _propTypes2.default.string,
  isStudent: _propTypes2.default.bool
};

CustomButton.defaultProps = {
  title: 'Button',
  onPress: function onPress() {},
  width: 118,
  height: 50,
  disabled: false,
  secondary: false,
  secondaryTextColor: null,
  isStudent: false
};

exports.CustomButton = CustomButton;