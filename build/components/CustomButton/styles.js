'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactNative = require('react-native');

var _constants = require('../../constants');

exports.default = styles = _reactNative.StyleSheet.create({
  container: {
    borderRadius: 4,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center'
  },
  gradientContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  secondaryButton: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: _constants.theme.spacing.unit * 2
  },
  text: {
    textDecorationLine: 'underline',
    color: _constants.theme.color.default.medium
  },
  primaryText: {
    color: 'white',
    fontWeight: _constants.theme.font.weight.bold,
    fontSize: _constants.theme.font.size.medium
  },
  primaryButton: {
    zIndex: 1,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  }
});