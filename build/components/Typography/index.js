'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Typography = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Typography = function Typography(props) {
  var children = props.children,
      style = props.style;

  return _react2.default.createElement(
    _reactNative.Text,
    { style: [style, _styles2.default.text] },
    children
  );
};

Typography.propTypes = {
  children: _propTypes2.default.node,
  style: _propTypes2.default.any
};

Typography.defaultProps = {
  children: '',
  style: {}
};

exports.Typography = Typography;