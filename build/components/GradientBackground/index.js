'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GradientBackground = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNativeSvg = require('react-native-svg');

var _reactNativeSvg2 = _interopRequireDefault(_reactNativeSvg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var GradientBackground = function GradientBackground(props) {
  var color1 = props.color1,
      color2 = props.color2;

  return _react2.default.createElement(
    _reactNativeSvg2.default,
    {
      height: '100%',
      width: '100%'
    },
    _react2.default.createElement(
      _reactNativeSvg.Defs,
      null,
      _react2.default.createElement(
        _reactNativeSvg.LinearGradient,
        {
          id: 'grad',
          x1: '0',
          y1: '0',
          x2: '100%',
          y2: '100%'
        },
        _react2.default.createElement(_reactNativeSvg.Stop, {
          offset: '0',
          stopColor: color1,
          stopOpacity: '1'
        }),
        _react2.default.createElement(_reactNativeSvg.Stop, {
          offset: '1',
          stopColor: color2,
          stopOpacity: '1'
        })
      )
    ),
    _react2.default.createElement(_reactNativeSvg.Rect, {
      x: '0',
      y: '0',
      width: '100%',
      height: '100%',
      fill: 'url(#grad)'
    })
  );
};

exports.GradientBackground = GradientBackground;