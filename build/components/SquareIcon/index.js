'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SquareIcon = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SquareIcon = function SquareIcon(props) {
  var renderIcon = props.renderIcon,
      backgroundColor = props.backgroundColor,
      disabled = props.disabled,
      onPress = props.onPress;


  return _react2.default.createElement(
    _reactNative.TouchableOpacity,
    {
      disabled: disabled,
      onPress: onPress
    },
    _react2.default.createElement(
      _reactNative.View,
      {
        style: [_styles2.default.container, { backgroundColor: backgroundColor }]
      },
      renderIcon()
    )
  );
};

SquareIcon.propTypes = {
  renderIcon: _propTypes2.default.func.isRequired,
  backgroundColor: _propTypes2.default.string,
  disabled: _propTypes2.default.bool,
  onPress: _propTypes2.default.func
};

SquareIcon.defaultProps = {
  backgroundColor: _constants.theme.color.blue.medium,
  disabled: true,
  onPress: function onPress() {}
};

exports.SquareIcon = SquareIcon;