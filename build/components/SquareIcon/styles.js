'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactNative = require('react-native');

var _constants = require('../../constants');

exports.default = styles = _reactNative.StyleSheet.create({
  container: {
    borderRadius: 4,
    aspectRatio: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: _constants.theme.spacing.unit / 2
  },
  button: {
    alignItems: 'center'
  }
});