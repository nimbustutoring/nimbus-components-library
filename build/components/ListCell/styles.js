'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactNative = require('react-native');

var _constants = require('../../constants');

exports.default = styles = _reactNative.StyleSheet.create({
  card: {
    width: '100%',
    padding: _constants.theme.spacing.unit * 2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  textContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    fontSize: _constants.theme.font.size.medium,
    color: _constants.theme.color.default.medium,
    paddingHorizontal: _constants.theme.spacing.unit * 2,
    fontWeight: _constants.theme.font.weight.bold
  },
  titleContainer: {
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  subtext: {
    fontSize: _constants.theme.font.size.small,
    color: _constants.theme.color.default.medium,
    paddingHorizontal: _constants.theme.spacing.unit * 2,
    fontWeight: _constants.theme.font.weight.medium,
    paddingTop: _constants.theme.spacing.unit
  }
});