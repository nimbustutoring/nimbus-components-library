'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ListCell = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _constants = require('../../constants');

var _components = require('../../components');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ListCell = function ListCell(props) {
  var title = props.title,
      renderLeftLogo = props.renderLeftLogo,
      renderRightLogo = props.renderRightLogo,
      subTitle = props.subTitle;


  var shouldRenderLeft = renderLeftLogo();
  var shouldRenderRight = renderRightLogo();
  var bold = _constants.theme.font.weight.medium;
  var light = _constants.theme.font.weight.light;

  return _react2.default.createElement(
    _components.Card,
    { style: _styles2.default.card },
    _react2.default.createElement(
      _reactNative.View,
      { style: _styles2.default.textContainer },
      shouldRenderLeft && renderLeftLogo(),
      _react2.default.createElement(
        _reactNative.View,
        { style: _styles2.default.titleContainer },
        _react2.default.createElement(
          _components.Typography,
          {
            style: [_styles2.default.text, { fontWeight: subTitle ? bold : light }]
          },
          title
        ),
        subTitle && _react2.default.createElement(
          _components.Typography,
          { style: _styles2.default.subtext },
          subTitle
        )
      )
    ),
    shouldRenderRight && renderRightLogo()
  );
};

ListCell.propTypes = {
  title: _propTypes2.default.string,
  subTitle: _propTypes2.default.string,
  renderRightLogo: _propTypes2.default.func,
  renderLeftLogo: _propTypes2.default.func
};

ListCell.defaultProps = {
  title: 'Title',
  subTitle: undefined,
  renderRightLogo: function renderRightLogo() {
    return undefined;
  },
  renderLeftLogo: function renderLeftLogo() {
    return undefined;
  }
};

exports.ListCell = ListCell;