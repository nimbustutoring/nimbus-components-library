'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Day = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Typography = require('../Typography');

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _constants = require('../../constants');

var _SquareIcon = require('../SquareIcon');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Day = function (_React$Component) {
  _inherits(Day, _React$Component);

  function Day(props) {
    _classCallCheck(this, Day);

    var _this = _possibleConstructorReturn(this, (Day.__proto__ || Object.getPrototypeOf(Day)).call(this, props));

    _this.state = {};
    _this.renderIcon = _this.renderIcon.bind(_this);
    return _this;
  }

  _createClass(Day, [{
    key: '_getTextStyle',
    value: function _getTextStyle() {
      var color = _constants.theme.color,
          font = _constants.theme.font;
      var type = this.props.type;


      switch (type) {
        case 'available':
          return { color: color.default.dark };
          break;
        case 'selected':
          return { color: 'white' };
          break;
        case 'default':
          return { color: color.default.medium };
          break;
        case 'previous':
          return { color: color.default.light };
          break;
        default:
          return;
      }
    }
  }, {
    key: '_getBorderStyle',
    value: function _getBorderStyle() {
      var blue = _constants.theme.color.blue;
      var type = this.props.type;


      switch (type) {
        case 'available':
          return blue.light;
          break;
        case 'selected':
          return blue.medium;
          break;
        default:
          return;
      }
    }
  }, {
    key: 'renderIcon',
    value: function renderIcon() {
      var _props = this.props,
          text = _props.text,
          type = _props.type;

      return _react2.default.createElement(
        _Typography.Typography,
        { style: [this._getTextStyle(), _styles2.default.text] },
        text
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          text = _props2.text,
          type = _props2.type,
          booked = _props2.booked;

      var disabled = type === 'previous' ? true : false;

      return _react2.default.createElement(
        _reactNative.View,
        { style: _styles2.default.button },
        _react2.default.createElement(_SquareIcon.SquareIcon, {
          backgroundColor: this._getBorderStyle(),
          renderIcon: this.renderIcon,
          disabled: false
        }),
        booked && _react2.default.createElement(_reactNative.View, { style: _styles2.default.ball })
      );
    }
  }]);

  return Day;
}(_react2.default.Component);

;

Day.propTypes = {
  text: _propTypes2.default.string,
  disabled: _propTypes2.default.bool,
  type: _propTypes2.default.string,
  booked: _propTypes2.default.bool,
  onPress: _propTypes2.default.func
};

Day.defaultProps = {
  text: '',
  disabled: false,
  type: 'default',
  booked: false,
  onPress: function onPress() {}
};

exports.Day = Day;