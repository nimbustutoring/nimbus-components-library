'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactNative = require('react-native');

var _constants = require('../../constants');

exports.default = styles = _reactNative.StyleSheet.create({
  container: {
    borderRadius: 5,
    aspectRatio: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: _constants.theme.font.size.large,
    fontWeight: _constants.theme.font.weight.bold
  },
  ball: {
    height: 6,
    width: 6,
    borderRadius: 3,
    marginTop: _constants.theme.spacing.unit / 2,
    backgroundColor: _constants.theme.color.blue.dark
  },
  button: {
    alignItems: 'center'
  }
});