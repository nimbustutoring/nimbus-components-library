'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactNative = require('react-native');

var _constants = require('../../constants');

exports.default = styles = _reactNative.StyleSheet.create({
  root: {
    width: '100%'
  },
  progressContainer: {
    backgroundColor: _constants.theme.color.default.light,
    width: '100%',
    height: 5,
    borderRadius: 2.5
  },
  progress: {
    height: 5,
    borderRadius: 2.5
  },
  stepContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between'
  },
  step: {
    flex: 1,
    margin: _constants.theme.spacing.unit / 2
  }
});