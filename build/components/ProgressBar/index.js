'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ProgressBar = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ProgressBar = function ProgressBar(props) {
  var color = props.color,
      progressWidth = props.progressWidth,
      steps = props.steps,
      total = props.total,
      progress = props.progress;


  var renderSteps = function renderSteps() {
    return _lodash2.default.range(total).map(function (o) {
      return _react2.default.createElement(_reactNative.View, {
        key: o,
        style: [_styles2.default.progress, _styles2.default.step, { backgroundColor: o < progress ? color : _constants.theme.color.default.light }]
      });
    });
  };

  return _react2.default.createElement(
    _reactNative.View,
    { style: _styles2.default.root },
    !steps && _react2.default.createElement(
      _reactNative.View,
      { style: _styles2.default.progressContainer },
      _react2.default.createElement(_reactNative.View, {
        style: [_styles2.default.progress, { width: progressWidth, backgroundColor: color }]
      })
    ),
    steps && _react2.default.createElement(
      _reactNative.View,
      { style: _styles2.default.stepContainer },
      renderSteps()
    )
  );
};

ProgressBar.propTypes = {
  progressWidth: _propTypes2.default.string, // does not apply to steps
  color: _propTypes2.default.string,
  steps: _propTypes2.default.bool,
  total: _propTypes2.default.number,
  progress: _propTypes2.default.number
};

ProgressBar.defaultProps = {
  progressWidth: '0%',
  color: _constants.theme.color.green.medium,
  steps: false,
  total: 1,
  progress: 0
};

exports.ProgressBar = ProgressBar;