'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Separator = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Separator = function Separator(props) {
  var backgroundColor = props.backgroundColor,
      width = props.width;


  return _react2.default.createElement(_reactNative.View, { style: [_styles2.default.line, { backgroundColor: backgroundColor, width: width }] });
};

Separator.propTypes = {
  backgroundColor: _propTypes2.default.string,
  width: _propTypes2.default.any
};

Separator.defaultProps = {
  backgroundColor: _constants.theme.color.default.light,
  width: '100%'
};

exports.Separator = Separator;