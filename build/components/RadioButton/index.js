'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RadioButton = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _constants = require('../../constants');

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var RadioButton = function RadioButton(_ref) {
  var selected = _ref.selected,
      onPress = _ref.onPress,
      isStudent = _ref.isStudent;

  var color = isStudent ? _constants.theme.color.orange.light : _constants.theme.color.blue.medium;
  return _react2.default.createElement(
    _reactNative.TouchableOpacity,
    { onPress: onPress },
    _react2.default.createElement(
      _reactNative.View,
      { style: [_styles2.default.outterCircle, { borderColor: color }] },
      selected && _react2.default.createElement(_reactNative.View, { style: [_styles2.default.circle, { backgroundColor: color }] })
    )
  );
};

RadioButton.propTypes = {
  selected: _propTypes2.default.bool,
  onPress: _propTypes2.default.func,
  isStudent: _propTypes2.default.bool
};

RadioButton.defaultProps = {
  selected: false,
  onPress: function onPress() {},
  isStudent: false
};

exports.RadioButton = RadioButton;