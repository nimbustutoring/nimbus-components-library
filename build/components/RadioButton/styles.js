'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactNative = require('react-native');

var _constants = require('../../constants');

exports.default = styles = _reactNative.StyleSheet.create({
  outterCircle: {
    width: 19,
    height: 19,
    borderColor: _constants.theme.color.blue.medium,
    borderRadius: 9.5,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  circle: {
    width: 10,
    height: 10,
    backgroundColor: _constants.theme.color.blue.medium,
    borderRadius: 5
  }
});