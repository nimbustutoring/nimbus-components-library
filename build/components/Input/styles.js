'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactNative = require('react-native');

var _constants = require('../../constants');

exports.default = styles = _reactNative.StyleSheet.create({
  container: {
    borderWidth: 1,
    borderRadius: 4,
    height: 50,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  text: {
    width: '80%',
    fontSize: _constants.theme.font.size.medium,
    fontFamily: _constants.theme.font.family.default
  },
  innerContainer: {
    height: '100%',
    width: _constants.theme.spacing.unit * 6,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden'
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1,
    padding: _constants.theme.spacing.unit
  }
});