'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Input = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

{/* for phone number use this function to normalize phone number (514) 928-3838
   normalize(phone) {
     if (phone.length === 10) {
       Keyboard.dismiss(); <- remember to import keyboard
       return phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
     }
     return null;
   }
  */}

var Input = function (_React$Component) {
  _inherits(Input, _React$Component);

  function Input(props) {
    _classCallCheck(this, Input);

    var _this = _possibleConstructorReturn(this, (Input.__proto__ || Object.getPrototypeOf(Input)).call(this, props));

    _this.state = {
      color: _constants.theme.color.default.light,
      value: '',
      fontWeight: _constants.theme.font.weight.light
    };
    _this.onFocus = _this.onFocus.bind(_this);
    _this.onEndEditing = _this.onEndEditing.bind(_this);
    _this.onChangeText = _this.onChangeText.bind(_this);
    return _this;
  }

  _createClass(Input, [{
    key: 'onFocus',
    value: function onFocus() {
      var color = _constants.theme.color,
          font = _constants.theme.font;

      var final = this.props.isStudent ? color.orange.light : color.blue.medium;
      this.setState({ color: final, fontWeight: font.weight.medium });
    }
  }, {
    key: 'onEndEditing',
    value: function onEndEditing() {
      var _this2 = this;

      var value = this.state.value;
      var weight = _constants.theme.font.weight;

      var color = value === '' ? _constants.theme.color.default.light : _constants.theme.color.default.medium;
      var fontWeight = value === '' ? weight.light : weight.medium;
      this.setState({ color: color, fontWeight: fontWeight }, function () {
        _this2.props.onEndEditing(value);
      });
    }
  }, {
    key: 'onChangeText',
    value: function onChangeText(text) {
      var _this3 = this;

      this.setState({ value: text }, function () {
        _this3.props.onChangeText(_this3.state.value);
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this4 = this;

      var _props = this.props,
          leftLogo = _props.leftLogo,
          rightLogo = _props.rightLogo,
          textLogo = _props.textLogo,
          placeholder = _props.placeholder,
          secureTextEntry = _props.secureTextEntry,
          keyboardType = _props.keyboardType;
      var _state = this.state,
          color = _state.color,
          value = _state.value,
          fontWeight = _state.fontWeight;


      var renderTextLogo = textLogo();
      var renderLeftLogo = leftLogo();
      var renderRightLogo = rightLogo();

      return _react2.default.createElement(
        _reactNative.View,
        { style: [_styles2.default.container, { borderColor: color }] },
        renderLeftLogo && _react2.default.createElement(
          _reactNative.View,
          {
            style: [_styles2.default.innerContainer, { borderColor: color, borderRightWidth: 1 }]
          },
          leftLogo()
        ),
        _react2.default.createElement(
          _reactNative.View,
          { style: _styles2.default.inputContainer },
          _react2.default.createElement(_reactNative.TextInput, {
            placeholder: placeholder,
            placeholderTextColor: color,
            style: [_styles2.default.text, { color: color, fontWeight: fontWeight }],
            onFocus: this.onFocus,
            onEndEditing: this.onEndEditing,
            onChangeText: function onChangeText(text) {
              return _this4.onChangeText(text);
            },
            secureTextEntry: secureTextEntry,
            keyboardType: keyboardType,
            value: value
          }),
          renderTextLogo && textLogo()
        ),
        renderRightLogo && _react2.default.createElement(
          _reactNative.View,
          {
            style: [_styles2.default.innerContainer, { borderColor: color, borderLeftWidth: 1 }]
          },
          rightLogo()
        )
      );
    }
  }]);

  return Input;
}(_react2.default.Component);

;

Input.propTypes = {
  leftLogo: _propTypes2.default.func,
  rightLogo: _propTypes2.default.func,
  textLogo: _propTypes2.default.func,
  placeholder: _propTypes2.default.string,
  onEndEditing: _propTypes2.default.func,
  onChangeText: _propTypes2.default.func,
  secureTextEntry: _propTypes2.default.bool,
  keyboardType: _propTypes2.default.string,
  isStudent: _propTypes2.default.bool
};

Input.defaultProps = {
  leftLogo: function leftLogo() {
    return undefined;
  },
  rightLogo: function rightLogo() {
    return undefined;
  },
  textLogo: function textLogo() {
    return undefined;
  },
  placeholder: 'This is a PlaceHolder',
  onEndEditing: function onEndEditing() {},
  onChangeText: function onChangeText() {},
  secureTextEntry: false,
  keyboardType: "default",
  isStudent: false
};

exports.Input = Input;