'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InputText = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _constants = require('../../constants');

var _Typography = require('../Typography');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var InputText = function InputText(props) {
  var title = props.title,
      value = props.value;

  var leftPadding = _constants.theme.spacing.unit;
  return _react2.default.createElement(
    _reactNative.View,
    { style: _styles2.default.container },
    _react2.default.createElement(
      _reactNative.View,
      { style: _styles2.default.titleContainer },
      _react2.default.createElement(
        _Typography.Typography,
        { style: _styles2.default.text },
        title
      )
    ),
    _react2.default.createElement(
      _Typography.Typography,
      { style: [_styles2.default.text, { paddingLeft: leftPadding }] },
      value
    )
  );
};

InputText.propTypes = {
  title: _propTypes2.default.string,
  value: _propTypes2.default.string
};

InputText.defaultProps = {};

exports.InputText = InputText;