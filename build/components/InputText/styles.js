'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactNative = require('react-native');

var _constants = require('../../constants');

exports.default = styles = _reactNative.StyleSheet.create({
  container: {
    borderRadius: 4,
    width: '100%',
    borderBottomWidth: 1,
    borderColor: _constants.theme.color.default.light,
    flexDirection: 'row'
  },
  text: {
    color: _constants.theme.color.default.dark,
    fontSize: _constants.theme.font.size.large,
    paddingRight: _constants.theme.spacing.unit,
    paddingBottom: _constants.theme.spacing.unit / 2
  },
  titleContainer: {
    borderRightWidth: 1,
    borderColor: _constants.theme.color.default.light
  }
});