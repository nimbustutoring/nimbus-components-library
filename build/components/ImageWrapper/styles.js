'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _outterCircle;

var _reactNative = require('react-native');

var _constants = require('../../constants');

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

exports.default = styles = _reactNative.StyleSheet.create({
  container: {
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    aspectRatio: 1
  },
  innerCircle: {
    width: 18,
    height: 18,
    borderRadius: 9
  },
  outterCircle: (_outterCircle = {
    bottom: 0,
    width: 20,
    height: 20,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: 'white',
    backgroundColor: 'white',
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    right: -5
  }, _defineProperty(_outterCircle, 'bottom', -5), _defineProperty(_outterCircle, 'overflow', 'hidden'), _outterCircle)
});