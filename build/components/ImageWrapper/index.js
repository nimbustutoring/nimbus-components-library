'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ImageWrapper = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getSize = function getSize(size) {
  switch (size) {
    case 'large':
      return 60;
      break;
    case 'medium':
      return 45;
      break;
    case 'small':
      return 35;
      break;
    default:
      return null;
  }
};

var renderOrg = function renderOrg(renderLogo) {
  var blue = _constants.theme.color.blue;

  var shouldRenderLogo = renderLogo() === null ? false : true;

  if (shouldRenderLogo) return _react2.default.createElement(
    _reactNative.View,
    { style: _styles2.default.outterCircle },
    renderLogo()
  );
};

var ImageWrapper = function ImageWrapper(props) {
  var children = props.children,
      size = props.size,
      renderLogo = props.renderLogo,
      active = props.active;

  var width = getSize(size);

  return _react2.default.createElement(
    _reactNative.View,
    null,
    _react2.default.createElement(
      _reactNative.View,
      { style: [_styles2.default.container, { width: width, borderRadius: width / 2 }] },
      children
    ),
    renderOrg(renderLogo)
  );
};

ImageWrapper.propTypes = {
  children: _propTypes2.default.node,
  size: _propTypes2.default.string,
  renderLogo: _propTypes2.default.func,
  active: _propTypes2.default.any //bool
};

ImageWrapper.defaultProps = {
  children: '',
  size: 'small',
  renderLogo: function renderLogo() {
    return null;
  },
  active: true
};

exports.ImageWrapper = ImageWrapper;