'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactNative = require('react-native');

var _constants = require('../../constants');

exports.default = styles = _reactNative.StyleSheet.create({
  container: {
    borderColor: _constants.theme.color.blue.medium,
    borderRadius: 14,
    borderWidth: 3.5,
    width: 40,
    height: 25,
    justifyContent: 'center'
  },
  ball: {
    borderColor: _constants.theme.color.blue.medium,
    borderRadius: 7,
    borderWidth: 3.5,
    width: 14,
    height: 14,
    margin: _constants.theme.spacing.unit / 4
  }
});