'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Switch = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactNative = require('react-native');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = require('./styles');

var _styles2 = _interopRequireDefault(_styles);

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Switch = function (_React$Component) {
  _inherits(Switch, _React$Component);

  function Switch(props) {
    _classCallCheck(this, Switch);

    var _this = _possibleConstructorReturn(this, (Switch.__proto__ || Object.getPrototypeOf(Switch)).call(this, props));

    _this.state = {
      on: false
    };
    _this.renderInner = _this.renderInner.bind(_this);
    _this.onPress = _this.onPress.bind(_this);
    return _this;
  }

  _createClass(Switch, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.setState({ on: this.props.on });
    }
  }, {
    key: 'renderInner',
    value: function renderInner() {
      return _react2.default.createElement(_reactNative.View, { style: _styles2.default.ball });
    }
  }, {
    key: 'onPress',
    value: function onPress() {
      var _this2 = this;

      this.setState({ on: !this.state.on }, function () {
        return _this2.props.onPress(_this2.state.on);
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var on = this.state.on;

      var align = on ? 'flex-end' : 'flex-start';
      var opacity = on ? 1 : 0.5;

      return _react2.default.createElement(
        _reactNative.TouchableOpacity,
        { onPress: this.onPress },
        _react2.default.createElement(
          _reactNative.View,
          { style: [_styles2.default.container, { alignItems: align, opacity: opacity }] },
          this.renderInner()
        )
      );
    }
  }]);

  return Switch;
}(_react2.default.Component);

;

Switch.propTypes = {
  on: _propTypes2.default.bool,
  onPress: _propTypes2.default.func
};

Switch.defaultProps = {
  on: false,
  onPress: function onPress() {}
};

exports.Switch = Switch;