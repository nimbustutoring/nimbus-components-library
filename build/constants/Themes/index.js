'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var theme = {
  color: {
    blue: {
      light: 'rgb(209.1, 232.05, 244.8)',
      medium: 'rgb(53.55, 183.6, 229.5)',
      dark: 'rgb(2.55, 168.3, 229.5)',
      gradientStart: '#32B6E5',
      gradientEnd: '#7BCDEA'
    },
    orange: {
      light: 'rgb(255, 124.95, 124.95)',
      medium: 'rgb(237.15, 127.5, 119.85)',
      dark: 'rgb(216.75, 117.3, 117.3)',
      gradientStart: 'rgb(237, 128, 120)',
      gradientEnd: 'rgb(242, 148, 140)'
    },
    green: {
      light: 'rgb(117.3, 216.75, 122.4)',
      medium: 'rgb(102, 211.65, 153)'
    },
    red: {
      light: 'rgb(255, 124.95, 124.95)'
    },
    default: {
      backgroundGrey: 'rgb(243,244,248)',
      light: 'rgb(186.15, 188.7, 193.8)',
      medium: 'rgb(158.1, 158.1, 163.2)',
      dark: 'gray',
      black: 'rgb(56.1, 56.1, 56.1)'
    }
  },
  font: {
    size: {
      small: 12,
      medium: 14,
      large: 18
    },
    weight: {
      light: '100',
      medium: '600',
      bold: '900'
    },
    family: {
      default: 'Nunito-Regular'
    }
  },
  spacing: {
    unit: 8
  }
};

exports.theme = theme;