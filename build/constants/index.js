'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Themes = require('./Themes');

Object.keys(_Themes).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _Themes[key];
    }
  });
});