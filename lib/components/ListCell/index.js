"use strict";

import React from "react";
import { TouchableOpacity, View, Image } from "react-native";
import PropTypes from "prop-types";

import { theme } from "../../constants";
import { Typography } from "../Typography";
import { Card } from "../Card";
import styles from "./styles";

const ListCell = (props) => {
  const {
    title,
    renderLeftLogo,
    renderRightLogo,
    subtitle,
    onPress,
    style,
    disabled,
    selected,
    testID,
    subtitleNoWrap,
    titleStyle,
    subtitleStyle,
    type,
    typeTitle,
    renderBelow,
    intrestedStudents,
  } = props;

  const shouldRenderLeft = renderLeftLogo();
  const shouldRenderRight = renderRightLogo();
  const shouldRenderBelow = renderBelow();

  const cellBody = (param) => {
    switch (param) {
      case "private":
        return (
          <View
            style={styles.titleContainer}
            accessibilityLabel={`${typeTitle}. ${title}. ${subtitle ? subtitle : ''}`}
          >
            <Typography style={styles.typeText}>{typeTitle}</Typography>
            <Typography style={styles.text}>{title}</Typography>
            {subtitle && (
              <Typography
                noWrap={subtitleNoWrap}
                style={[styles.subtext, subtitleStyle]}
                testID={subtitle}
              >
                {subtitle}
              </Typography>
            )}
          </View>
        );
      case "dropIn":
        return (
          <View
            style={styles.titleContainer}
            accessibilityLabel={`${typeTitle}. ${title}. ${subtitle ? subtitle : ''}`}
          >
            <Typography style={styles.typeText}>{typeTitle}</Typography>
            <Typography style={styles.text}>{title}</Typography>
            {subtitle && (
              <Typography
                noWrap={subtitleNoWrap}
                style={[styles.subtext, subtitleStyle]}
                testID={subtitle}
              >
                {subtitle}
              </Typography>
            )}
            <Typography style={styles.typeText} testID={intrestedStudents}>{intrestedStudents}</Typography>
          </View>
        );
      default:
        return (
          <View style={styles.titleContainer} accessibilityLabel={`${title}. ${subtitle ? subtitle : ''}`}>
            <Typography noWrap style={[styles.text, titleStyle]}>
              {title}
            </Typography>
            {subtitle && (
              <Typography
                noWrap={subtitleNoWrap}
                style={[styles.subtext, subtitleStyle]}
                testID={subtitle}
              >
                {subtitle}
              </Typography>
            )}
          </View>
        );
    }
  };

  return (
    <View>
      <TouchableOpacity disabled={disabled} onPress={onPress} testID={testID}>
        <Card style={[styles.card, style, selected && styles.selected]}>
          <View style={styles.textContainer}>
            {shouldRenderLeft && renderLeftLogo()}
            {cellBody(type)}
          </View>
          {shouldRenderRight && renderRightLogo()}
        </Card>
        {shouldRenderBelow && renderBelow()}
      </TouchableOpacity>
    </View>
  );
};

ListCell.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  renderRightLogo: PropTypes.func,
  renderLeftLogo: PropTypes.func,
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
  selected: PropTypes.bool,
  subtitleNoWrap: PropTypes.bool,
  titleStyle: PropTypes.object,
  subtitleStyle: PropTypes.object,
  testID: PropTypes.string,
  type: PropTypes.string,
  typeIcon: PropTypes.func,
  renderBelow: PropTypes.func,
  intrestedStudents: PropTypes.string,
  typeTitle: PropTypes.string,
};

ListCell.defaultProps = {
  title: "Title",
  subtitle: undefined,
  renderRightLogo: () => undefined,
  renderLeftLogo: () => undefined,
  onPress: () => {},
  disabled: false,
  selected: false,
  subtitleNoWrap: true,
  titleStyle: {
    fontSize: theme.font.size.medium,
    color: theme.color.default.medium,
  },
  subtitleStyle: { fontSize: 10, color: theme.color.default.medium },
  testID: "cell",
  type: "",
  typeTitle: "",
  renderBelow: () => undefined,
  intrestedStudents: "",
};

export { ListCell };
