import { StyleSheet } from "react-native";
import { theme } from "../../constants";

export default StyleSheet.create({
  card: {
    width: "100%",
    padding: theme.spacing.unit,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  textContainer: {
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
    maxWidth: "80%",
  },
  text: {
    marginHorizontal: theme.spacing.unit,
    fontWeight: theme.font.weight.bold,
  },
  titleContainer: {
    justifyContent: "center",
    alignItems: "flex-start",
  },
  subtext: {
    marginHorizontal: theme.spacing.unit,
    fontWeight: theme.font.weight.medium,
    // marginTop: theme.spacing.unit,
    // paddingTop: theme.spacing.unit,
  },
  selected: {
    borderWidth: 2,
  },
  typeText: {
    marginHorizontal: theme.spacing.unit,
    fontSize: 11,
  },
});
