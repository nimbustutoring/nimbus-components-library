'use strict';

import React from 'react';
import { StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';
import { theme } from '../../constants';
import { Typography } from '../Typography';

const InputText = (props) => {
  const { title, value } = props;
  const leftPadding = theme.spacing.unit;
  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Typography style={styles.text}>{title}</Typography>
      </View>
      <Typography style={[styles.text, {paddingLeft: leftPadding}]}>
        {value}
      </Typography>
    </View>
  );
};

InputText.propTypes = {
  title: PropTypes.string,
  value: PropTypes.string
};

InputText.defaultProps = {

};

export { InputText };
