import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

export default StyleSheet.create({
  container: {
    borderRadius: 4,
    width: '100%',
    borderBottomWidth: 1,
    borderColor: theme.color.default.light,
    flexDirection: 'row',
  },
  text: {
    color: theme.color.default.dark,
    fontSize: theme.font.size.large,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit / 2
  },
  titleContainer: {
    borderRightWidth: 1,
    borderColor: theme.color.default.light
  },
});
