'use strict';

import React from 'react';
import { StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';
import { theme } from '../../constants';

const Separator = (props) => {
  const { backgroundColor, width } = props;

  return (
    <View style={[styles.line, { backgroundColor, width }]} />
  );
};

Separator.propTypes = {
  backgroundColor: PropTypes.string,
  width: PropTypes.any,
}

Separator.defaultProps = {
  backgroundColor: 'rgb(230, 230, 230)',
  width: '100%',
}

export { Separator };
