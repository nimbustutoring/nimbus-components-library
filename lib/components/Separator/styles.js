import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

export default StyleSheet.create({
  line: {
    width: '100%',
    height: 1,
  }
});
