import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

export default StyleSheet.create({
  text: {
    color: 'white',
    fontWeight: theme.font.weight.bold,
    fontSize: theme.font.size.small,
  },
  container: {
    width: 40,
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 12.5,
  },
});
