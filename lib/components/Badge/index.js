'use strict';

import React from 'react';
import { StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';
import { theme } from '../../constants';
import { Typography } from '../Typography';

const Badge = (props) => {
  const { backgroundColor, number, size } = props;

  return (
    <View style={[styles.container, { backgroundColor }]}>
      <Typography style={styles.text}>{number}</Typography>
    </View>
  );
};

Badge.propTypes = {
  number: PropTypes.number,
  backgroundColor: PropTypes.string,
};

Badge.defaultProps = {
  number: 0,
  backgroundColor: theme.color.blue.dark,
};

export { Badge };
