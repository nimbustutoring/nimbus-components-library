import { StyleSheet } from "react-native";
import { theme } from "../../constants";

export default StyleSheet.create({
  container: {
    backgroundColor: "white",
    borderRadius: 6,
  },
  shadow: {
    shadowOffset: { width: 5, height: 5 },
    shadowColor: theme.color.default.dark,
    shadowOpacity: 0.2,
    elevation: 2,
  },
});
