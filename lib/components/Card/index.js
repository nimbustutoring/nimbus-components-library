"use strict";

import React from "react";
import { StyleSheet, View } from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import { theme } from "../../constants";

const Card = ({ children, style, showShadow, testID }) => (
  <View
    style={[styles.container, style, showShadow && styles.shadow]}
    testID={testID}
  >
    {children}
  </View>
);

Card.propTypes = {
  children: PropTypes.node,
  style: PropTypes.any,
  testID: PropTypes.string,
};

Card.defaultProps = {
  children: "",
  style: {},
  testID: "card",
};

export { Card };
