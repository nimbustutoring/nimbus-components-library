'use strict';

import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

const Typography = ({ children, style, onLayout, noWrap, numberOfLines, textProps, testID }) => (
  <Text
    testID={testID}
    accessibilityLabel={`${children}`}
    onLayout={onLayout}
    style={[styles.text, style]}
    numberOfLines={noWrap ? 1 : numberOfLines}
    { ...textProps }
  >
    {children}
  </Text>
);

Typography.propTypes = {
  children: PropTypes.node,
  style: PropTypes.any,
  onLayout: PropTypes.func,
  noWrap: PropTypes.bool,
  numberOfLines: PropTypes.number,
  testID: PropTypes.string,
};

Typography.defaultProps = {
  children: '',
  style: {},
  onLayout: () => {},
  noWrap: false,
  numberOfLines: undefined,
  onPress: () => {},
  testID: 'typography',
};

export { Typography };
