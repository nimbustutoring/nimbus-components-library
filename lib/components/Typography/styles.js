import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

export default StyleSheet.create({
  text: {
    fontFamily: theme.font.family.default,
  }
});
