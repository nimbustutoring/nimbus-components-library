import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

const SIZE = theme.spacing.unit * 4;

export default StyleSheet.create({
  container: {
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    width: SIZE,
    height: SIZE,
    padding: theme.spacing.unit / 2,
  },
});
