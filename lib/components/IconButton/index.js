'use strict';

import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

const IconButton = props => {
  const { backgroundColor, children, disabled, onPress, testID } = props;

  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      style={[styles.container, { backgroundColor }]}
      hitSlop={{ top: 5, left: 5, bottom: 5, right: 5 }}
      testID={testID}
      accessibilityLabel={testID}
    >
      {children}
    </TouchableOpacity>
  );
}

IconButton.propTypes = {
  children: PropTypes.any.isRequired,
  backgroundColor: PropTypes.string,
  disabled: PropTypes.bool,
  onPress: PropTypes.func,
  testID: PropTypes.string,
};

IconButton.defaultProps = {
  backgroundColor: 'transparent',
  disabled: false,
  onPress: () => {},
  testID: 'iconButton'
};

export { IconButton };
