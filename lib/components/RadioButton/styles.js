import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

export default StyleSheet.create({
  outterCircle: {
    width: 19,
    height: 19,
    borderColor: theme.color.blue.medium,
    borderRadius: 9.5,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  circle: {
    width: 10,
    height: 10,
    backgroundColor: theme.color.blue.medium,
    borderRadius: 5,
  },
});
