'use strict';

import React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import { theme } from '../../constants';
import styles from './styles';

const RadioButton = ({ selected, onPress, disabled, isStudent, color }) => {
  return (
    <TouchableOpacity onPress={onPress} disabled={disabled}>
      <View style={[styles.outterCircle, { borderColor: color }]}>
        { selected && <View style={[styles.circle, { backgroundColor: color }]}/> }
      </View>
    </TouchableOpacity>
  );
};

RadioButton.propTypes = {
  selected: PropTypes.bool,
  onPress: PropTypes.func,
  isStudent: PropTypes.bool,
  disabled: PropTypes.bool,
  color: PropTypes.any,
}

RadioButton.defaultProps = {
  selected: false,
  onPress: () => {},
  isStudent: false,
  disabled: false,
  color: theme.color.blue.medium,
}

export { RadioButton };
