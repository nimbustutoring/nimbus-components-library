'use strict';

import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';

import styles from './styles';
import { theme } from '../../constants';

const getSize = size => {
  switch (size) {
    case 'large':
      return 60;
      break;
    case 'medium':
      return 45;
      break;
    case 'small':
      return 35;
      break;
    default:
      return null;
  }
};

const renderOrg = (logo, size) => {
  const { blue } = theme.color;
  const marginLeft = getSize(size) / 1.45;
  return (
    <View style={[styles.outterCircle, { marginLeft }]}>
      <Image style={styles.logo} source={{ uri: logo }} />
    </View>
  );
};

const ImageWrapper = ({ children, size, logo, active, shadow, source }) => {
  const width = getSize(size);
  const style=[styles.container, { width, borderRadius: width / 2 }, shadow && styles.shadow];
  const uri = typeof source === 'string' ? source : source.path;
  const logoUri = typeof logo === 'string' ? logo : logo.path;
  return (
    <View>
      {isEmpty(source) && (
        <View style={style}>
          {children}
        </View>
      )}
      {!isEmpty(source) && (
        <Image key={uri} style={style} source={{ uri }} />
      )}
      {!isEmpty(logo) && renderOrg(logoUri, size)}
    </View>
  );
};

ImageWrapper.propTypes = {
  children: PropTypes.node,
  size: PropTypes.string,
  logo: PropTypes.string,
  active: PropTypes.any, //bool
  shadow: PropTypes.bool,
  source: PropTypes.any,
};

ImageWrapper.defaultProps = {
  children: null,
  size: 'small',
  logo: '',
  active: true,
  shadow: false,
  source: '',
};

export { ImageWrapper };
