import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

export default StyleSheet.create({
  container: {
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    aspectRatio: 1,
  },
  innerCircle: {
    width: 22,
    height: 22,
    borderRadius: 11,
  },
  outterCircle: {
    bottom: 0,
    width: 24,
    height: 24,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: 'white',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -20,
    overflow: 'hidden',
    zIndex: 1,
  },
  shadow: {
    shadowColor: 'black',
    shadowOffset: { width: 3, height: 3 },
    shadowRadius: 5,
    shadowOpacity: 1.0,
    elevation: 2,
  },
  logo: {
    height: 24,
    width: 24,
  },
});
