import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderRadius: 4,
    height: 24,
    padding: theme.spacing.unit / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontWeight: theme.font.weight.medium,
    alignSelf: 'center'
  },
  button: {
    marginLeft: theme.spacing.unit,
    alignItems: 'center',
    justifyContent: 'center',
  },
  texts: {
    fontSize: theme.font.size.small - 2,
    color: 'lightgrey',
    marginLeft: theme.spacing.unit,
  },
});
