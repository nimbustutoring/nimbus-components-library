"use strict";

import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import { theme } from "../../constants";
import { Typography } from "../Typography";

const Chip = ({
  backgroundColor,
  text,
  textColor,
  onDelete,
  opacity,
  rightLogo,
  textSize,
  borderWidth,
  borderColor,
}) => {
  const hitSlop = { top: 4, bottom: 4, left: 6, right: 6 };
  return (
    <View
      style={[
        styles.container,
        { borderWidth, borderColor, backgroundColor, opacity },
      ]}
    >
      <Typography
        style={[styles.text, { color: textColor, fontSize: textSize }]}
      >
        {text}
      </Typography>
      {rightLogo && (
        <TouchableOpacity
          hitSlop={hitSlop}
          style={styles.button}
          onPress={onDelete}
        >
          {rightLogo}
        </TouchableOpacity>
      )}
    </View>
  );
};

Chip.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  textColor: PropTypes.string,
  onDelete: PropTypes.func,
  opacity: PropTypes.number,
  rightLogo: PropTypes.node,
  textSize: PropTypes.number,
  borderWidth: PropTypes.number,
  borderColor: PropTypes.string,
};

Chip.defaultProps = {
  backgroundColor: theme.color.blue.medium,
  text: "Bachelors",
  textColor: "white",
  onDelete: null,
  opacity: 1,
  rightLogo: null,
  textSize: theme.font.size.small,
  borderWidth: 0,
  borderColor: "",
};

export { Chip };
