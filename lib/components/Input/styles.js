import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

export default StyleSheet.create({
  container: {
    borderWidth: 1,
    borderRadius: 4,
    height: 45,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  text: {
    width: '80%',
    fontSize: theme.font.size.medium,
    fontFamily: theme.font.family.default,
    height: '100%',
  },
  innerContainer: {
    height: '100%',
    width: theme.spacing.unit * 6,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1,
    padding: theme.spacing.unit
  }
});
