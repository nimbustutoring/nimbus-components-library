'use strict';

import React from 'react';
import { StyleSheet, TextInput, TouchableWithoutFeedback, View } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';
import { theme } from '../../constants';

{/* for phone number use this function to normalize phone number (514) 928-3838
  normalize(phone) {
    if (phone.length === 10) {
      Keyboard.dismiss(); <- remember to import keyboard
      return phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
    }
    return null;
  }
*/}

class Input extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: props.default ? 'white' : theme.color.default.light,
      value: '',
      fontWeight: theme.font.weight.light,
    }

    this._focus = this._focus.bind(this);
    this._blur = this._blur.bind(this);
    this._onFocus = this._onFocus.bind(this);
    this._onEndEditing = this._onEndEditing.bind(this);
    this._onChangeText = this._onChangeText.bind(this);
  }

  componentWillReceiveProps({ value }) {
    this.setState({ value });
  }

  _focus() {
    this._onFocus();
    this._textInput.focus();
  }

  _blur() {
    this._textInput.blur();
  }

  _onFocus() {
    const { color, font } = theme;
    const final = this.props.color;
    const c = this.props.default ? 'white' : final
    this.setState({ color: c, fontWeight: font.weight.bold });
    this.props.onFocus();
  }

  _onEndEditing() {
    const { value } = this.state;
    const { weight } = theme.font;
    const color = value === '' ? theme.color.default.light : theme.color.default.medium;
    const c = this.props.default ? 'white' : color;
    const fontWeight = value === '' ? weight.light : weight.bold;
    this.setState({ color: c, fontWeight }, () => {
      this.props.onEndEditing(value);
    });
  }

  _onChangeText(text) {
    this.props.onChangeText(text);
  }

  render() {
    const {
      leftLogo,
      rightLogo,
      textLogo,
      placeholder,
      secureTextEntry,
      keyboardType,
      autoCapitalize,
      textInputProps,
      testID,
      rightBackgroundColor,
      placeholderTextColor,
      borderColor,
      backgroundColor,
      fontColor,

    } = this.props;

    const { color, value, fontWeight } = this.state;

    const renderTextLogo = textLogo();
    const renderLeftLogo = leftLogo();
    const renderRightLogo = rightLogo();

    let rightStyle = { borderColor: color, borderLeftWidth: 1 };
    if (rightBackgroundColor) rightStyle.backgroundColor = rightBackgroundColor;
    rightStyle = [styles.innerContainer, rightStyle];

    return (
      <TouchableWithoutFeedback onPress={this._focus}>
        <View style={[styles.container, { borderColor: borderColor || color, backgroundColor }]}>
          {renderLeftLogo && (
            <View style={[styles.innerContainer, { borderColor: borderColor || color, borderRightWidth: 1 }]}>
              {leftLogo()}
            </View>
          )}
          <View style={styles.inputContainer}>
            <TextInput
              ref={node => this._textInput = node}
              placeholder={placeholder}
              placeholderTextColor={placeholderTextColor || color}
              style={[styles.text, { color: fontColor || color, fontWeight }]}
              onFocus={this._onFocus}
              onEndEditing={this._onEndEditing}
              onChangeText={text => this._onChangeText(text)}
              secureTextEntry={secureTextEntry}
              keyboardType={keyboardType}
              value={value}
              autoCapitalize={autoCapitalize}
              underlineColorAndroid="transparent"
              {...textInputProps}
              testID={testID}
              accessibilityLabel={testID}
            />
            {renderTextLogo && textLogo()}
          </View>
          {renderRightLogo && (
            <View style={rightStyle}>
              {rightLogo()}
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
    );
  }
};

Input.propTypes = {
  leftLogo: PropTypes.func,
  rightLogo: PropTypes.func,
  textLogo: PropTypes.func,
  placeholder: PropTypes.string,
  onEndEditing: PropTypes.func,
  onChangeText: PropTypes.func,
  onFocus: PropTypes.func,
  secureTextEntry: PropTypes.bool,
  keyboardType: PropTypes.string,
  isStudent: PropTypes.bool,
  autoCapitalize: PropTypes.string,
  textInputProps: PropTypes.object,
  rightBackgroundColor: PropTypes.string,
  default: PropTypes.bool,
  color: PropTypes.any,
  placeholderTextColor: PropTypes.string,
  borderColor: PropTypes.string,
  backgroundColor: PropTypes.string,
  fontColor: PropTypes.string,
  testID: PropTypes.string,
};

Input.defaultProps = {
  leftLogo: () => undefined,
  rightLogo: () => undefined,
  textLogo: () => undefined,
  placeholder: 'This is a PlaceHolder',
  onEndEditing: () => {},
  onChangeText: () => {},
  onFocus: () => {},
  secureTextEntry: false,
  keyboardType: 'default',
  isStudent: false,
  autoCapitalize: 'sentences',
  textInputProps: {},
  rightBackgroundColor: '',
  default: false,
  color: theme.color.blue.medium,
  placeholderTextColor: undefined,
  borderColor: undefined,
  backgroundColor: undefined,
  fontColor: undefined,
  testID: 'inputField',
};

export { Input };
