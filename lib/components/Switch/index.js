'use strict';

import React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';
import { theme } from '../../constants';

class Switch extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      on: false,
    };

    this.renderInner = this.renderInner.bind(this);
    this.onPress = this.onPress.bind(this);
  }

  componentDidMount(){
    this.setState({ on: this.props.on });
  }

  renderInner() {
    const { color } = this.props;
    return (
      <View style={[styles.ball, { borderColor: color }]} />
    );
  }

  onPress() {
    this.setState({ on: !this.state.on }, () => this.props.onPress(this.state.on));
  }

  render() {
    const { on } = this.state;
    const align = on ? 'flex-end' : 'flex-start';
    const opacity = on ? 1 : 0.2;
    const { color } = this.props;
    const { disable } = this.props;
    return (
      <TouchableOpacity
        onPress={this.onPress}
        disabled={disable}
      >
        <View style={[styles.container, { alignItems: align, opacity, borderColor: color }]}>
          {this.renderInner()}
        </View>
      </TouchableOpacity>
    );
  }
};

Switch.propTypes = {
  on: PropTypes.bool,
  onPress: PropTypes.func,
  color: PropTypes.any,
  disable: PropTypes.bool
};

Switch.defaultProps = {
  on: false,
  onPress: () => {},
  color: theme.color.blue.medium,
  disable: false
};

export { Switch };
