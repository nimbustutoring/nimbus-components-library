import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

export default StyleSheet.create({
  container: {
    borderColor: theme.color.blue.medium,
    borderRadius: 14,
    borderWidth: 3.5,
    width: 40,
    height: 25,
    justifyContent: 'center',
  },
  ball: {
    borderColor: theme.color.blue.medium,
    borderRadius: 7,
    borderWidth: 3.5,
    width: 14,
    height: 14,
    margin: theme.spacing.unit / 4,
  }
});
