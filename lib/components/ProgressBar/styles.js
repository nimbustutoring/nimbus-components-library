import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

export default StyleSheet.create({
  root: {
    width: '100%',
  },
  progressContainer: {
    backgroundColor: theme.color.default.light,
    width: '100%',
    height: 5,
    borderRadius: 2.5,
  },
  progress: {
    height: 5,
    borderRadius: 2.5,
  },
  stepContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between'
  },
  step: {
    flex: 1,
    margin: theme.spacing.unit / 2,
  }
});
