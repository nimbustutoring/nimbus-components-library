'use strict';

import React from 'react';
import { StyleSheet, View, } from 'react-native';
import PropTypes from 'prop-types';

import _ from 'lodash';

import styles from './styles';
import { theme } from '../../constants';

const ProgressBar = (props) => {
  const {
    color,
    progressWidth,
    steps,
    total,
    progress,
  } = props;

  const renderSteps = () =>
    _.range(total).map((o) =>
        <View
          key={o}
          style={[
            styles.progress,
            styles.step,
            { backgroundColor: o < progress ? color : theme.color.default.light }
          ]}
        />
      );

  return (
    <View style={styles.root}>
      {
        !steps &&
        <View style={styles.progressContainer}>
          <View
            style={[
              styles.progress,
              { width: progressWidth, backgroundColor: color }
            ]}
          />
        </View>
      }
      {
        steps &&
        <View style={styles.stepContainer}>
          { renderSteps() }
        </View>
      }
    </View>
  );
};

ProgressBar.propTypes = {
  progressWidth: PropTypes.string, // does not apply to steps
  color: PropTypes.string,
  steps: PropTypes.bool,
  total: PropTypes.number,
  progress: PropTypes.number,
};

ProgressBar.defaultProps = {
  progressWidth: '0%',
  color: theme.color.green.medium,
  steps: false,
  total: 1,
  progress: 0,
};

export { ProgressBar };
