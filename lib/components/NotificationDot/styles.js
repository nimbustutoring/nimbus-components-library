import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

export default StyleSheet.create({
  dot: {
    width: 8,
    height: 8,
    borderRadius: 4,
  }
});
