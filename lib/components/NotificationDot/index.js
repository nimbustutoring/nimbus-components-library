'use strict';

import React from 'react';
import { StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';
import { theme } from '../../constants';

const NotificationDot = (props) => {
  const { color } = props;

  return (
    <View style={[styles.dot, { backgroundColor: color }]}/>
  );
};

NotificationDot.propTypes = {
  color: PropTypes.string,
};

NotificationDot.defaultProps = {
  color: theme.color.red.light,
};

export { NotificationDot };
