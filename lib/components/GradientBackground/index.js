'use strict';

import React from 'react';
import Svg, { Defs, LinearGradient, Rect, Stop } from 'react-native-svg';

const GradientBackground = (props) => {
  const { color1, color2 } = props;
  return (
    <Svg
      height="100%"
      width="100%"
    >
      <Defs>
        <LinearGradient
          id="grad"
          x1="0"
          y1="0"
          x2="100%"
          y2="100%"
        >
          <Stop
            offset="0"
            stopColor={color1}
            stopOpacity="1"
          />
          <Stop
            offset="1"
            stopColor={color2}
            stopOpacity="1"
          />
        </LinearGradient>
      </Defs>
      <Rect
        x="0"
        y="0"
        width="100%"
        height="100%"
        fill="url(#grad)"
      />
    </Svg>
  );
}

export { GradientBackground };
