'use strict';

import React from 'react';
import {
  View,
  TouchableWithoutFeedback,
  PanResponder,
  Animated,
  Slider,
} from 'react-native';
import PropTypes from 'prop-types';

import { theme } from '../../constants';
import styles from './styles';

class Slide extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      size: 20,
      pan: new Animated.ValueXY(),
      scale: new Animated.Value(1),
    };
    this._onPressIn = this._onPressIn.bind(this);
    this._onPressOut = this._onPressOut.bind(this);
  }

  componentWillMount() {
    const { pan } = this.state;

    this._val = { x:0, y:0 }
    pan.addListener((value) => this._val = value);

    this.panResponder = PanResponder.create({
      onMoveShouldSetResponderCapture: () => true,
      onMoveShouldSetPanResponderCapture: () => true,

      //initially set x and y valuej
      onPanResponderGrant: (e, gestureState) => {
        pan.setOffset({x: pan.x._value, y: 0});
        pan.setValue({x: 0, y: 0});
        Animated.spring(
          this.state.scale,
          { toValue: 1.2, friction: 3 }
        ).start();
      },

      onPanResponderMove: (e, gestureState)=> {
        console.log(pan.x)
        pan.x._value === undefined ?
          null : Animated.event([ null, {dx: pan.x, dy: 0}, ])
          (e, gestureState)
      },

      onPanResponderRelease: (e, {vx, vy}) => {
        // Flatten the offset to avoid erratic behavior
        pan.flattenOffset();
        Animated.spring(
          this.state.scale,
          { toValue: 1, friction: 3 }
        ).start();
      }
    });
  }

  _onPressIn() {
    this.setState({ size: 22 })
  }

  _onPressOut() {
    this.setState({ size: 20 })
  }

  _renderButtonThumb() {
    const { size } = this.state;
    return (
      <View
        style={[
          styles.buttonOutter,
          { width: size, height: size, borderRadius: size/2 }
        ]}
      >
        <View
          style={[
            styles.buttonInner,
            { width: size/2, height: size/2, borderRadius: size/4 }
          ]}/>
      </View>
    );
  }

  _renderButton() {
    const { pan, scale } = this.state;
    const { dark } = theme.color.blue;
    const [translateX] = [pan.x];
    const animatedStyle = {transform: [{translateX}, {scale}]};

    return (
      <Animated.View
        {...this.panResponder.panHandlers}
        style={[animatedStyle, styles.animatedContainer]}
      >
      {this._renderButtonThumb()}
      </Animated.View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        {this._renderButton()}
      </View>
    );
  }
};

Slide.propTypes = {
};

Slide.defaultProps = {
};

export { Slide };
