import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

export default StyleSheet.create({
  container: {
    backgroundColor: theme.color.default.light,
    width: '100%',
    height: theme.spacing.unit / 1.6,
    borderRadius: 10,
    justifyContent: 'center'
  },
  buttonOutter : {
      backgroundColor: theme.color.blue.medium,
      alignItems: 'center',
      justifyContent: 'center',
  },
  buttonInner: {
    backgroundColor: 'white',
  },
  animatedContainer: {
    flex: 1,
    width: 0,
    backgroundColor: theme.color.blue.medium,
    justifyContent: 'center',
  }
});
