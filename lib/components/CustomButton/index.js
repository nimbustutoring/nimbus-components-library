'use strict';

import React from 'react';
import { StyleSheet, Button, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import { GradientBackground } from '../GradientBackground'
import { Typography } from '../Typography';
import styles from './styles';
import { theme } from '../../constants';
import { toUpper } from 'lodash';
import { Card } from 'react-native-nimbus-components/lib/components/Card';

const CustomButton = (props) => {
  const {
    title,
    onPress,
    width,
    height,
    disabled,
    secondary,
    secondaryTextColor,
    isStudent,
    style,
    textStyle,
    buttonStyle,
    onIn,
    testID,
    gradient,
  } = props;

  const renderPrimaryButton = () => {
    const start = gradient.gradientStart;
    const end = gradient.gradientEnd;
    return (
      <GradientBackground
        color1={start}
        color2={end}
      />
    );
  }

  return (
    <View style={style}>
      {!secondary && (
        <View style={[styles.container, { width, height }]}>
          <TouchableOpacity
            disabled={disabled}
            onPress={onIn ? undefined : onPress}
            onPressIn={onIn ? onPress : undefined}
            style={[styles.primaryButton, { width, height }]}
          >
            <Typography
            style={styles.primaryText}
            testID={testID}
            >
              {toUpper(title)}
            </Typography>
          </TouchableOpacity>
          {renderPrimaryButton()}
        </View>
      )}
      {secondary && (
        <TouchableOpacity
          disabled={disabled}
          style={[styles.secondaryButton, buttonStyle]}
          onPress={onIn ? undefined : onPress}
          onPressIn={onIn ? onPress : undefined}
          testID={testID}
        >
          <Typography style={[styles.text, { color: secondaryTextColor }, textStyle]}>
            {title}
          </Typography>
        </TouchableOpacity>
      )}
    </View>
  );
};

CustomButton.propTypes = {
  title: PropTypes.string,
  onPress: PropTypes.func,
  width: PropTypes.any,
  height: PropTypes.any,
  disabled: PropTypes.bool,
  secondary: PropTypes.bool,
  secondaryTextColor: PropTypes.string,
  isStudent: PropTypes.bool,
  style: PropTypes.any,
  textStyle: PropTypes.any,
  buttonStyle: PropTypes.any,
  onIn: PropTypes.bool,
  testID: PropTypes.string,
};

CustomButton.defaultProps = {
  title: 'Button',
  onPress: () => {},
  width: 118,
  height: 50,
  disabled: false,
  secondary: false,
  secondaryTextColor: theme.color.default.dark,
  isStudent: false,
  style: {},
  textStyle: {},
  buttonStyle: {},
  onIn: false,
  testID: 'buttonConfirm',
};

export { CustomButton };
