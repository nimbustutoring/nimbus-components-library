import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

export default StyleSheet.create({
  container: {
    borderRadius: 4,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  gradientContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  secondaryButton: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing.unit * 2
  },
  text: {
    textDecorationLine: 'underline',
    fontWeight: theme.font.weight.bold,
    color: theme.color.default.light,
  },
  primaryText: {
    color: 'white',
    fontWeight: theme.font.weight.bold,
    fontSize: theme.font.size.medium,
    alignSelf: 'center',
    textAlign: 'center'
  },
  primaryButton: {
     zIndex: 1,
     position: 'absolute',
     alignItems: 'center',
     justifyContent: 'center',
  },
});
